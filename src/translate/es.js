module.exports = {
  'likes': '{likes, number} me gusta',
  'logout': 'Salir',
  'english': 'Ingles',
  'spanish': 'Español',
  'signup.subheading': 'Regístrate para ver a tus amigos estudiando en Platzi',
  'signup.facebook': 'Iniciar sesión en Facebook',
  'signup.text':'Iniciar sesión',
  'email': 'Correo electrónico',
  'username': 'Nombre de usuario',
  'fullname': 'Nombre completo',
  'password': 'Contraseña',
  'signup.call-to-action': 'Regístrate',
  'signup.have-account': '¿Tienes una cuenta?',
  'signin' : 'Entrar',
  'signin.not-have-account': '¿No tienes una cuenta?',
  'language': 'Idioma'

}
